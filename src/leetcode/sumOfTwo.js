/*
  Sum Of Two

  You have two integer arrays, a and b, and an integer target value v.
  Determine whether there is a pair of numbers,
  where one number is taken from a and the other from b,
  that can be added together to get a sum of v.

  Return true if such a pair exists, otherwise return false
*/

// Option 1
// Slow - Nested loops
const sumOfTwo = (a, b, v) => {
  let result = false;

  a.forEach(numA => {
    const temp = b.some(numB => numA + numB === v);
    if (temp) result = temp;
  });

  return result;
};

// Option 2
// Optimum - two loops, but not nested
const sumOfTwoAlt = (a, b, v) => {
  const compliments = a.map(element => v - element);
  const intersection = b.filter(element => compliments.includes(element));

  return !!intersection.length;
};



const a = [1,2,3];
const b = [10,20,30,40];
const v = 42;

const aa = [0,0,-5, 30212];
const bb = [-10,40,-3,9];
const vv = -8;

console.log(sumOfTwo(a, b, v)); // true
console.log(sumOfTwoAlt(a, b, v)); // true

console.log(sumOfTwo(aa, bb, vv)); // true
console.log(sumOfTwoAlt(aa, bb, vv)); // true
//
console.log(sumOfTwo([], [], vv)); // false
console.log(sumOfTwoAlt([], [], vv)); // false
