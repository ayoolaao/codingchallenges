/*
* This Question was asked during my Google technical interview

* Given a non-empty array of integers, return the count (not value) of the most frequent element.
*
* Example 1:
* Input: nums = [1,1,1,2,2,3], k = 2
* Output: [1,2]
*
* Example 2:
* Input: nums = [1], k = 1
* Output: [1]
*
* Note:
* You may assume k is always valid, 1 ≤ k ≤ number of unique elements.
* Your algorithm's time complexity must be better than O(n log n), where n is the array's size.
* It's guaranteed that the answer is unique, in other words the set of the top k frequent elements is unique.
* You can return the answer in any order.
*
* */

export const topFrequentElementInArray = (nums, k) => {
  let currentMostFrequent;

  // Map array elements to Hash
  let myHash = new Map();

  for (let num of nums) {
    if (myHash.has(num)) myHash.set(num, myHash.get(num) + 1);
    else myHash.set(num, 1);
  }

  // Get new array of [values] from map
  let myVArray = Array.from(myHash.values());

  currentMostFrequent = myVArray[0];

  for (let i = 1; i < myVArray.length; i += 1) if (myVArray[i] > currentMostFrequent) currentMostFrequent = myVArray[i];

  return currentMostFrequent;

  //? Solution is O(n)
};

