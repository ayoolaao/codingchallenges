const fibonaciGen = n => {
  if (n === 1) return 0;
  if (n === 2) return 1;

  let seq = [0, 1]
  seq.forEach((e, i) => {
    if (i !== n) {
      seq.push(seq[i - 1] + i)
    } else {
      return
    }
  })
};
