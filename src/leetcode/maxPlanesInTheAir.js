/*
 * Given an interval list which are flying and landing time of the flight. How many airplanes are on the sky at most?
 *
 * Notice: If landing and flying happens at the same time, we consider landing should happen at first.
 *
 * Example: For interval list
 * [
 *   [1,10],
 *   [2,3],
 *   [5,8],
 *   [4,7]
 * ]
 *
 * return 3
 */

/*
 * Solution:
 * declare currentFlightsInAir Array
 * declare maxFlights to keep track of max flights in the air - use MinHeap here to optimize
 * Sort flights by start time
 * loop through flights
 *    * remove any flights that have landed from currentFlightsInAir
 *    * Add flight to list of flights in currentFlightsInAir
 *    * Update maxFlights?
 * return maxFlights
 */

export const maxPlanesInTheAir = flights => {
  let maxFlight = 0;

  flights.sort((a, b) => b[0] - a[0]);

  return maxFlight;
};
