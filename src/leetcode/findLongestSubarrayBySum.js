/*
 * Given an array arr[] of size n containing integers. The problem is to find the longest sub-array having sum equal to the given value s.
 *
 * Example:
 * Input:  arr = [1,2,3,4,5,0,0,0,6,7,8,9,10], s = 15
 * Output: [1, 8]
*/

export const findLongestSubarrayBySum = (arr, s) => {
  let result = [-1];

  let left = 0;
  let right = 0;
  let currentSum = 0;

  while (right < arr.length) {
    currentSum += arr[right];
    while (left < right && currentSum > s) {
      currentSum -= arr[left++];
    }

    if (currentSum === s && (result.length === 1 || result[1] - result[0] < right - left)) {
      result = [left + 1, right + 1];
    }

    right += 1;
  }

  return result;
};
