/*
 * Product of Array Except Self
 * Given an array nums of n integers where n > 1,  return an array output such that output[i] is equal to the product of all the elements of nums except nums[i].
 *
 * Example:
 * Input:  [1,2,3,4]
 * Output: [24,12,8,6]
 *
 * Constraint: It's guaranteed that the product of the elements of any prefix or suffix of the array (including the whole array) fits in a 32 bit integer.
 *
 * Note: Please solve it without division and in O(n).
 *
 * Follow up:
 * Could you solve it with constant space complexity? (The output array does not count as extra space for the purpose of space complexity analysis.)
*/

// Simple but with O(3n) === O(n)
export const productExceptSelf = nums => {
  let N = nums.length;

  let leftProducts = [];
  let rightProducts = [];

  let final = [];

  leftProducts[0] = 1;
  rightProducts[N-1] = 1;

  for (let i = 1; i < N; i ++) leftProducts[i] = nums[i - 1] * leftProducts[i - 1];
  for (let i = N - 2; i >= 0; i --) rightProducts[i] = nums[i + 1] * rightProducts[i + 1];
  for (let i = 0; i < N; i ++) final[i] = leftProducts[i] * rightProducts[i];

  return final;
};

// Better but with O(3n) === O(n)
export const productExceptSelfAdv = nums => {
  let N = nums.length;

  let final = [];
  final[0] = 1;

  for (let i = 1; i < N; i ++) final[i] = nums[i - 1] * final[i - 1];

  let R = 1;
  for (let i = N - 1; i >= 0; i --) {
    final[i] = final[i] * R;
    R = R * nums[i];
  }

  return final;
};
