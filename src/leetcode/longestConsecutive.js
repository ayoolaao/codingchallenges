/*
  Given an unsorted array of integers, find the length of the longest consecutive elements sequence.

  Your algorithm should run in O(n) complexity.

  Example:
  Input: [100, 4, 200, 1, 3, 2]
  Output: 4
  Explanation: The longest consecutive elements sequence is [1, 2, 3, 4]. Therefore its length is 4.
*/

export const longestConsecutive = nums => {
  const numsSet = new Set(nums);
  let longest = 0;

  for (let num of numsSet ) {
    if (numsSet.has(num - 1)) continue; //? We need to ensure we are starting with the first element in the sequence

    let currentNumber = num;
    let  currentLongest = 1;

    while (numsSet.has(currentNumber + 1)) {
      currentNumber += 1;
      currentLongest += 1;
    }

    longest = Math.max(longest, currentLongest);
  }

  return longest
};
