// opt 1 - 2n time
export const firstNonRepeatingCharacter = str => {
  const arr = str.split('');
  let obj = new Map();

  arr.forEach(elem => obj.has(elem) ? obj.set(elem, obj.get(elem) + 1) : obj.set(elem, 1));

  // console.log(obj);

  for (let [k, v] of obj) {
    if (v === 1) return k;
  }

  return -1;
};

// const data = 'aaaxbbcccddeee';

// const r = firstNonRepeatingCharacter(data);
// console.log(r);
