// Write a recursive sum function e.g sumR(1)(2)(3)(4)(5)......()

// Standard JS
export const sumR = (x) => {
  return (y) => {
    if (y) return sumR(x + y);
    return x;
  };
}

// ES6 One liner
export const sumRE = x => y => y ? sumRE(x + y) : x;

console.log(sumRE(1)(2)(3)(4)(5)(6)());



// Currying

// function volume1(length) {
//   return function(width) {
//     return function(height) {
//       return height * width * length;
//     }
//   }
// }
//
// volume1(2)(3)(4); // 24
