/*
* Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.
*
* Example:
* Input: [0,1,0,3,12]
* Output: [1,3,12,0,0]
*
* Note:
* You must do this in-place without making a copy of the array.
* Minimize the total number of operations.
* */

export const moveZeroes = nums => {
  if(nums.includes(0)) {
    let j = 0;
    for (let i = 0; i < nums.length; i++) {
      if (nums[i] !== 0) {
        nums[j] = nums[i];
        if (i>j) nums[i] = 0;
        j++;
      }
    }
  }

  return nums;
};

//* Create index placeholder
//* Loop over array, moving non zero elements to the start of array after the previously placed one (i.e keeping track of the index)
//* loop from index to length of array setting each element to zero

export const moveZeroesAdv = nums => {
  let index = 0;

  for (let i = 0; i < nums.length; i += 1) {
    const curNum = nums[i];

    if (curNum !== 0) {
      nums[index] = curNum;
      index += 1;
    }
  }

  for (let i = index; i < nums.length; i += 1) {
    nums[i] = 0;
  }

  return nums;
};
