export const primeChecker = num => {
  if (num === 0 || num === 1) return false;
  if (num === 2) return true;
  else {
    for (let x = 2; x < num; x++) {
      return num % x !== 0;
    }
  }
}
