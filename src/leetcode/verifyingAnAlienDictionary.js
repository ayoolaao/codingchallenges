// https://leetcode.com/problems/verifying-an-alien-dictionary/

export const isAlienSorted = (words, order) => {
  const orderMap = new Map(); //? Create Map() for constant time look up

  //? Convert string to Array and populate Map() structure: looks like {a => 1, b => 2 ...}
  order.split('').forEach((letter, index) => orderMap.set(letter, index));

  //? Loop thought words array, starting at index 1
  for (let i = 1; i < words.length; i += 1) {
    const previousWord = words[i - 1]; //
    const currentWord = words[i];

    if (orderMap.get(previousWord[0]) > orderMap.get(currentWord[0])) return false; //? Base case compare initial chars
    else if (previousWord[0] === currentWord[0]) {
      let pointer = 1; //? tracker
      while (previousWord[pointer] === currentWord[pointer]) pointer += 1; //? go to next tracker if letters are the same (Magic)
      if (!currentWord[pointer]) return false; //? if undefined .. out of bounds of array
      if (orderMap.get(previousWord[pointer]) > orderMap.get(currentWord[pointer])) return false;
    }
  }
  return true;
  // ? Time complexity is o(n)
  //? The inner while loop is not dependent of the size of words so doesn't not count to time
};
