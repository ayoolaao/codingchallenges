/*
  Given a sorted integer array without duplicates, return the summary of its ranges.

  Example 1:
  Input:  [0,1,2,4,5,7]
  Output: ["0->2","4->5","7"]
  Explanation: 0,1,2 form a continuous range; 4,5 form a continuous range.

  Example 2:
  Input:  [0,2,3,4,6,8,9]
  Output: ["0","2->4","6","8->9"]
  Explanation: 2,3,4 form a continuous range; 8,9 form a continuous range.
 */

export const summaryRanges = inputArray => {
  let n = 0;
  let m = 0;

  let result =[];

  for (let i = 0; i < inputArray.length; i++) {
    if (inputArray[i + 1] - inputArray[i] !== 1){
      if (n === m) result.push(`${inputArray[n]}`);
      else result.push(`${inputArray[n]}->${inputArray[m]}`);
      n = i + 1;
    }
    m = i + 1;
  }

  return result;
};


export const summaryRangesAdv = inputArray => {
  let result = [];
  let n = null;

  for (let i = 0; i < inputArray.length; i++) {
    if (n === null) n = inputArray[i];
    while (i + 1 < inputArray.length && inputArray[i] + 1 === inputArray[i + 1]) i++;
    if (n !== inputArray[i]) result.push(`${n}->${inputArray[i]}`);
    else result.push(`${inputArray[i]}`);
    n = null;
  }

  return result;
};
