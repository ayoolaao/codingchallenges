/*
* Given a non-empty array of integers, return the k most frequent elements.
*
* Example 1:
* Input: nums = [1,1,1,2,2,3], k = 2
* Output: [1,2]
*
* Example 2:
* Input: nums = [1], k = 1
* Output: [1]
*
* Note:
* You may assume k is always valid, 1 ≤ k ≤ number of unique elements.
* Your algorithm's time complexity must be better than O(n log n), where n is the array's size.
* It's guaranteed that the answer is unique, in other words the set of the top k frequent elements is unique.
* You can return the answer in any order.
*
* */

export const topKFrequent = (nums, k) => {
  // Is the array sorted?

  // Map array elements to Hash
  let myHash = new Map();

  for (let num of nums) {
    if (myHash.has(num)) myHash.set(num, myHash.get(num) + 1);
    else myHash.set(num, 1);
  }

  // Get new array of [key, val] from map
  let myKVArray = Array.from(myHash.entries()); // [[k, v], [k, v], [k, v]]

  // Sort new array by value in desc;
  myKVArray.sort((a, b) => b[1] - a[1]);

  return myKVArray.slice(0, k).map(elem => elem[0]);

  //? Solution is O(n log n) because of the sorting
};
