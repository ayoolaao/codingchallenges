export const rotateImage = twoDArray => {
  const N = twoDArray.length;

  // Step 1 - transpose Array i.e swap(array[i][j]. array[j][i])
  for (let i = 0; i < N; i++) {
    for (let j = i; j < N; j++) {
      let temp = twoDArray[i][j];
      twoDArray[i][j] = twoDArray[j][i];
      twoDArray[j][i] = temp;
    }
  }

  // flip horizontally i.e swap(array[i][j]. array[i][N - 1 -j])
  for (let i = 0; i < N; i++) {
    for (let j = 0; j < N / 2; j++) {
      let temp = twoDArray[i][j];
      twoDArray[i][j] = twoDArray[i][N - 1 - j];
      twoDArray[i][N - 1 - j] = temp;
    }
  }

  return twoDArray;
};

// const twoD = [[1,2,3],[4,5,6],[7,8,9]];
// const result = rotateImage(twoD);
// console.log(result);
