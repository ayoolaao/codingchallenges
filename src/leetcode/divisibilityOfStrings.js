const s = 'bcdbcdeebcdbcdee';
const t = 'bcdbcdee';

const isDivisible = (s, t) => {
  let count = 0;
  if (s.length % t.length !== 0 || s.length < t.length) return -1;

  while (s.includes(t)) {
    count ++;
    s = s.replace(t, '');
  }

  return s.length ? -1 : t.length;
};

const longestRepeatingString = t => {
  let t1 = t.slice(0, t.length/2);
  let t2 = t.slice(t.length/2);

  while (t1.length) {
    if (t1.length === 1 && t1 === t[1]) return t1.length;
    if (t1 === t2) return t1.length;

    t1 = t1.slice(0, -1);
    t2 = t2.slice(0, -1);
  }
  return t.length;
};

const result = isDivisible(s, t) > -1 ? longestRepeatingString(t) : -1;
console.log(result);


console.log(longestRepeatingString('brbr'));
