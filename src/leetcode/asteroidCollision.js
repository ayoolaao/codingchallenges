// https://leetcode.com/problems/asteroid-collision/

export const asteroidCollision = asteroids => {
  const stack = []; //? using array.prototype.pop to simulate stack

  //? loop through asteroids array
  for (let i = 0; i < asteroids.length; i += 1) {
    if (!stack.length || asteroids[i] > 0) stack.push(asteroids[i]); //? if stack is empty and still on 1st elem in asteroids just push to stack
    else {
      while (true) {
        let topAsteroid = stack[stack.length - 1];

        if (topAsteroid < 0) {
          stack.push(asteroids[i]);
          break;
        } else if (topAsteroid === asteroids[i] * -1) {
          stack.pop();
          break;
        } else if (topAsteroid > asteroids[i] * -1) {
          break;
        } else {
          stack.pop();

          if (!stack.length) {
            stack.push(asteroids[i]);
            break;
          }
        }
      }
    }
  }

  return stack;
};
