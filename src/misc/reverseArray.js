export const reverseArrayBasic = arr => {  // o(n) time and o(1) space
  const stack = [];

  for (let i = arr.length - 1; i >= 0; i -= 1) {
    stack.push(arr[i]);
  }

  return stack;
};
