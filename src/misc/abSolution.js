/*
Write a function solution that, given a string S consisting of N letters 'a' and/or 'b' returns true when all occurrences of letter 'a' are before all occurrences of letter 'b' and returns false otherwise.

Examples:

Given S = 'aabbb', return true.
Given S = 'ba', return false.
Given S = 'aaa', return true.
Given S = 'b', return true.
Given S = 'abba', return false.
 */

export const abSolution = s => {
  if (s.length === 1) return true;

  for (let i = 1; i < s.length; i += 1) if (s[i - 1] === 'b' && s[i] === 'a') return false;

  return true;
};
