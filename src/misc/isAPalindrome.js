export const isAPalindrome = str => [...str].reverse().join('') === str; // Time complexity: O(n). Space complexity: O(1)
