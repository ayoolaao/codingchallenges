/* Write an emitter class:
*
* emitter = new Emitter ();
*
* 1. Support subscribing to events.
* sub = emitter.subscribe('event_name', callback);
* sub2 = emitter.subscribe('event_name', callback2);
*
* 2. Support emitting events.
* This particular example should lead to the `callback` above being invoked with `foo` and `bar` as parameters.
* emitter.emit('event_name', foo, bar);
*
* 3. Support unsubscribing existing subscriptions by releasing them.
* sub.release();
*
* `sub` is the reference returned by `subscribe` above
* */

//* Using Class Syntax
class Emitter {
  subscriptions = new Map(); // use Map() over Object because key can be any type

  subscribe (eventName, callBack) {
    if (!this.subscriptions.has(eventName)) this.subscriptions.set(eventName, new Set()); // Use Set to prevent duplicates
    this.subscriptions.get(eventName).add(callBack);

    return {
      release: () => {
        const callbacks = this.subscriptions.get(eventName);
        callbacks.delete(callBack);

        if (callbacks.size === 0) this.subscriptions.delete(eventName);
      }
    };
  };

  emit (eventName, ...args) {
    const callbacks = this.subscriptions.get(eventName);

    if (!callbacks) return;

    for (let callback of callbacks) {
      callback(...args);
    }
  };
}

const emitter = new Emitter();

let sub1Called = [];
let sub2Called = [];

const sub1 = emitter.subscribe('event 1', (...args) => sub1Called.push(args));
const sub2 = emitter.subscribe('event 2', (...args) => sub1Called.push(args));

emitter.emit('event 1', 1, 2);
emitter.emit('event 1', 3, 4);

console.log(sub1Called);
sub1.release();
emitter.emit('event 1', 6, 7);
console.log(sub1Called);
