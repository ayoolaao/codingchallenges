/*
* Flattening multidimensional Arrays in JavaScript
*
* var myArray = [[1, 2],[3, 4, 5], [6, 7, 8, 9]] =>  [1, 2, 3, 4, 5, 6, 7, 8, 9]
*
*
* */

//! Solution 1: Using concat() and apply()
export const flattenConcatApply = myArray => {
  return [].concat.apply([], myArray);
}


//! Solution 2: Using reduce()
export const flattenReduce = myArray => {
  return myArray.reduce((previous, current) => previous.concat(current));
}

//! Solution 3: recursive generator function for deep flat
export function* flattenGenerator(array, depth=Infinity) {
  for (const item of array) {
    if (Array.isArray(item) && depth > 0) yield* flattenGenerator(item, depth - 1);
    else yield item;
  }
}

//! Solution 4: builtin flat()
export const flattenFlat = myArray => {
  return myArray.flat();
};


//! Solution 5:  O(N log N)
export const flatten = items => {
  const flat = [];

  items.forEach(item => {
    if (Array.isArray(item)) flat.push(...flatten(item));
    else flat.push(item);
  })

  return flat;
};
