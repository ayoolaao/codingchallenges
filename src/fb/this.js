let person = {
  name: 'ayoola',
  printName: function () {
    console.log(this); // person object
  },
  printAge: () => {
    console.log(this); // person object
  },
};

person.printName(); // person object
person.printAge(); // {}
