/*
 * Two Number Sum

 * Given an array of distinct integer values and an integer value that represents a target sum,
 * return two numbers such that they add up to a specific target.

 * Example:
 *   [3, 5, -4, 8, 11, 1, -1, 6], 10 => [11, -1]

 * Note:
 *   You may assume that each input would have exactly one solution, and you may not use the same element twice.
 */

/*
* Option 1: Use two for loops:
*   1. traverse the array with a for loop,
*   2. for each element in the array, loop again through the remainder of the array
*   3. Check for another element that sums of to the target
*
* Issue:
*   * The time complexity is O(n^2)
* */
export const twoNumberSumBruteForce = (nums, target) => {
  for (let index1 = 0; index1 < nums.length; index1 += 1) {
    for (let index2 = index1 + 1; index2 < nums.length; index2 += 1) {
      if (nums[index1] + nums[index2] === target) return [nums[index1], nums[index2]]; //? If returning the element values themselves
      // if (nums[index1] + nums[index2] === target) return [index1, index2]; //? If returning the element indexes
    }
  }

  return null;
};

/*
* Option 2 (If Sorted): Sort the array first the get O(n log n) time complexity:
*   1. Sort the array in ascending order i.e [-4, -1, 1,  3, 5,  6, 8, 11]
*   2. Loop through the array.
*         - Set a left and right pointer to the start and end of the array respectively
*         - check if left + right === target
*         - if yes return [left, right]
*         - else: nested if()
*             - if (left + right < target) increment left pointer
*             - else (i.e left + right > target) decrease right pointer
*
* Explanation:
*   * time complexity of sort is O(n log n)
*   * time complexity of loop is O(n)
*   * time complexity of algorithm is O(n log n)
* */
export const twoNumberLeftRightPointer = (nums, target) => {
  //? If array not sorted
  nums.sort((a, b) => a - b);

  let leftPointer = 0;
  let rightPointer = nums.length - 1;

  while (leftPointer !== rightPointer) {
    if (nums[leftPointer] + nums[rightPointer] === target) return [nums[leftPointer], nums[rightPointer]]; //? If returning the element values themselves
    // if (nums[leftPointer] + nums[rightPointer] === target) return [leftPointer, rightPointer]; //? if returning the indexes
    else {
      if (nums[leftPointer] + nums[rightPointer] < target) leftPointer += 1;
      else rightPointer -= 1;
    }
  }
}

/*
* Option 3 (Best): Use a Hash Structure - Map() in Javascript:
*   1. Create an empty Map()
*   2. Loop through the array.
*   3. For each element (x) in array
*       - check if element (y) is already in the Map, where y = t - x
*       - if true, return element (x) and element (y) inform of [x, y]
*       - else insert the element (x) into the array
*
* Explanation:
*   * Map() as lookup time complexity of O(1) - Constant time
*   * Only one loop is needed - O(n) time complexity
* */
export const twoNumberSumMap = (nums, target) => {
  let lookupTable = new Map();

  //? if returning the element values themselves
  for (let element of nums) {
    const match = target - element;

    if (lookupTable.has(match)) return [match, element]
    else lookupTable.set(element, 1);
  }

  //? if returning the indexes
  // for (let i = 0; i < nums.length; i += 1) {
  //   const match = target - nums[i];
  //
  //   if (lookupTable.has(match)) return [lookupTable.get(match), i];
  //   else lookupTable.set(nums[i], i);
  // }

  return null;
};
