const SpecReporter = require('jasmine-spec-reporter').SpecReporter;
const JasmineConsoleReporter = require('jasmine-console-reporter');

const jasmineConsoleReporter = new JasmineConsoleReporter({
  colors: 1,
  cleanStack: 1,
  verbosity: 4,
  listStyle: 'indent',
  timeUnit: 'ms',
  timeThreshold: { ok: 500, warn: 1000, ouch: 3000 },
  activity: true,
  emoji: true,
  beep: true
});

const specReporter = new SpecReporter({
  spec: {
    displayPending: true
  }
})

jasmine.getEnv().clearReporters();               // remove default reporter logs
jasmine.getEnv().addReporter(jasmineConsoleReporter);
