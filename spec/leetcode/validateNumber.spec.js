import { isNumber } from '../../src/leetcode/validateNumber';

describe('isNumber()', () => {
  it('"0" should be true', () => {
    expect(isNumber('0')).toBeTrue();
  });

  it('" 0.1 " should be true', () => {
    expect(isNumber(' 0.1 ')).toBeTrue();
  });

  it('"abc" should be false', () => {
    expect(isNumber('abc')).toBeFalse();
  });

  it('"1 a" should be false', () => {
    expect(isNumber('1 a')).toBeFalse();
  });

  it('"2e10" should be true', () => {
    expect(isNumber('2e10')).toBeTrue();
  });

  it('" -90e3   " should be true', () => {
    expect(isNumber(' -90e3   ')).toBeTrue();
  });

  it('" 1e" should be false', () => {
    expect(isNumber(' 1e')).toBeFalse();
  });

  it('"e3" should be false', () => {
    expect(isNumber('e3')).toBeFalse();
  });

  it('" 6e-1" should be true', () => {
    expect(isNumber(' 6e-1')).toBeTrue();
  });

  it('" 99e2.5 " should be false', () => {
    expect(isNumber(' 99e2.5 ')).toBeFalse();
  });

  it('"53.5e93" should be true', () => {
    expect(isNumber('53.5e93')).toBeTrue();
  });

  it('" --6 " should be false', () => {
    expect(isNumber(' --6 ')).toBeFalse();
  });

  it('"-+3" should be false', () => {
    expect(isNumber('-+3')).toBeFalse();
  });

  it('"95a54e53" should be false', () => {
    expect(isNumber('95a54e53')).toBeFalse();
  });
});
