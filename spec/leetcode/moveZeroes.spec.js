import {moveZeroes, moveZeroesAdv} from '../../src/leetcode/moveZeroes';

describe('moveZeroes()', () => {
  it('Input: [0,1,0,3,12], Output: [1,3,12,0,0]', () => {
    expect(moveZeroes([0,1,0,3,12])).toEqual([1,3,12,0,0]);
  });

  it('Input: [0,0,1], Output: [1,0,0]', () => {
    expect(moveZeroes([0,0,1])).toEqual([1,0,0]);
  });
});

describe('moveZeroesAdv()', () => {
  it('Input: [0,1,0,3,12], Output: [1,3,12,0,0]', () => {
    expect(moveZeroesAdv([0,1,0,3,12])).toEqual([1,3,12,0,0]);
  });

  it('Input: [0,0,1], Output: [1,0,0]', () => {
    expect(moveZeroesAdv([0,0,1])).toEqual([1,0,0]);
  });
});
