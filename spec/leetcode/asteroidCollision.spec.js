import { asteroidCollision } from '../../src/leetcode/asteroidCollision';

describe('asteroidCollision()', () => {
  it('Case 1', () => {
    const input = [5, 10, -5];
    const output = [5, 10];

    expect(asteroidCollision(input)).toEqual(output);
  });

  it('Case 2', () => {
    const input = [8, -8];
    const output = [];

    expect(asteroidCollision(input)).toEqual(output);
  });

  it('Case 3', () => {
    const input = [10, 2, -5];
    const output = [10];

    expect(asteroidCollision(input)).toEqual(output);
  });

  it('Case 4', () => {
    const input = [-2, -1, 1, 2];
    const output = [-2, -1, 1, 2];

    expect(asteroidCollision(input)).toEqual(output);
  });
});
