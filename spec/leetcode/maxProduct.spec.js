import {maxProduct} from '../../src/leetcode/maxProduct';

describe('maxProduct()', () => {
  it('Input: [2,3,-2,4]. Output: 6', () => {
    expect(maxProduct([2,3,-2,4])).toEqual(6)
  });

  it('Input: [-2,0,-1]. Output: 0', () => {
    expect(maxProduct([-2,0,-1])).toEqual(0)
  });

  it('Input: [0,2]. Output: 2', () => {
    expect(maxProduct([0,2])).toEqual(2)
  });

  it('Input: [3,-1,4]. Output: 4', () => {
    expect(maxProduct([3,-1,4])).toEqual(4)
  });

  it('Input: [-4, -3]. Output: 12', () => {
    expect(maxProduct([-4, -3])).toEqual(12)
  });
});
