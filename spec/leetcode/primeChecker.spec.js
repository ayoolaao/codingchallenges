import {primeChecker} from '../../src/leetcode/primeChecker';

describe('primeChecker test', () => {
  it('input 39 should return true', () => {
    expect(primeChecker(39)).toBe(true)
  });

  it('input 2 should return true', () => {
    expect(primeChecker(2)).toBe(true)
  });

  it('input 1 should return false', () => {
    expect(primeChecker(1)).toBe(false)
  });

  it('input 103 should return true', () => {
    expect(primeChecker(103)).toBe(true)
  });

  it('input 199 should return true', () => {
    expect(primeChecker(199)).toBe(true)
  });
});
