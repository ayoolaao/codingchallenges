import {sumR, sumRE} from '../../src/leetcode/recursiveSum';

describe('recursive SumR e.g sum(a)(b)(c)(d)()', () => {
  it('sum(1)(2)(3)() should return 6', () => {
    expect(sumR(1)(2)(3)()).toBe(6);
  });

  it('sumR(1)(2)(3)(4)() should return 10', () => {
    expect(sumR(1)(2)(3)(4)()).toBe(10);
  });

  it('sumR(1)(2)(3)(4)(5)() should return 15', () => {
    expect(sumR(1)(2)(3)(4)(5)()).toBe(15);
  });

  it('sumR(1)(2)(3)(4)(5)(6)() should return 21', () => {
    expect(sumR(1)(2)(3)(4)(5)(6)()).toBe(21);
  });
});

describe('recursive SumRE e.g sum(a)(b)(c)(d)()', () => {
  it('sumE(1)(2)(3)() should return 6', () => {
    expect(sumRE(1)(2)(3)()).toBe(6);
  });

  it('sumRE(1)(2)(3)(4)() should return 10', () => {
    expect(sumRE(1)(2)(3)(4)()).toBe(10);
  });

  it('sumRE(1)(2)(3)(4)(5)() should return 15', () => {
    expect(sumRE(1)(2)(3)(4)(5)()).toBe(15);
  });

  it('sumRE(1)(2)(3)(4)(5)(6)() should return 21', () => {
    expect(sumRE(1)(2)(3)(4)(5)(6)()).toBe(21);
  });
});
