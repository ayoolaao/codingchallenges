import {longestConsecutive} from '../../src/leetcode/longestConsecutive';

describe('longestConsecutive())', () => {
  it('Input: [100, 4, 200, 1, 3, 2] should Output: 4', () => {
    const inputData = [100, 4, 200, 1, 3, 2];

    expect(longestConsecutive(inputData)).toEqual(4);
  });

  it('Input: [1,2,0,1] should Output: 3', () => {
    const inputData = [1,2,0,1];

    expect(longestConsecutive(inputData)).toEqual(3);
  });

  it('Input: [9,1,4,7,3,-1,0,5,8,-1,6] should Output: 3', () => {
    const inputData = [9,1,4,7,3,-1,0,5,8,-1,6];

    expect(longestConsecutive(inputData)).toEqual(7);
  });
});
