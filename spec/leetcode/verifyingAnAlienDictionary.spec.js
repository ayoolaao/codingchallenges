import { isAlienSorted } from '../../src/leetcode/verifyingAnAlienDictionary';

describe('isAlienSorted', () => {
  it('Case 1', () => {
    const words = ['hello', 'leetcode'];
    const order = 'hlabcdefgijkmnopqrstuvwxyz';

    expect(isAlienSorted(words, order)).toBeTrue();
  });

  it('Case 2', () => {
    const words = ['word', 'world', 'row'];
    const order = 'worldabcefghijkmnpqstuvxyz';

    expect(isAlienSorted(words, order)).toBeFalse();
  });

  it('Case 3', () => {
    const words = ['apple', 'app'];
    const order = 'abcdefghijklmnopqrstuvwxyz';

    expect(isAlienSorted(words, order)).toBeFalse();
  });
});
