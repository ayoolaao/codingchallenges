import { firstNonRepeatingCharacter } from '../../src/leetcode/firstNonRepeatingCharacter';

describe('firstNonRepeatingCharacter() test', () => {
  it('s: "abacabad" should return "c"', () => {
    expect(firstNonRepeatingCharacter('abacabad')).toEqual('c');
  });

  it('s: "abacabaabacaba" should return "-1"', () => {
    expect(firstNonRepeatingCharacter('abacabaabacaba')).toEqual(-1);
  });

  it('s: "z" should return "z"', () => {
    expect(firstNonRepeatingCharacter('z')).toEqual('z');
  });

  it('s: "bcb" should return "c"', () => {
    expect(firstNonRepeatingCharacter('bcb')).toEqual('c');
  });

  it('s: "bcccccccb" should return "-1"', () => {
    expect(firstNonRepeatingCharacter('bcccccccb')).toEqual(-1);
  });

  it('s: "abcdefghijklmnopqrstuvwxyziflskecznslkjfabe" should return "d"', () => {
    expect(firstNonRepeatingCharacter('abcdefghijklmnopqrstuvwxyziflskecznslkjfabe')).toEqual('d');
  });

  it('s: "zzz" should return "-1"', () => {
    expect(firstNonRepeatingCharacter('zzz')).toEqual(-1);
  });

  it('s: "bcccccccccccccyb" should return "y"', () => {
    expect(firstNonRepeatingCharacter('bcccccccccccccyb')).toEqual('y');
  });

  it('s: "xdnxxlvupzuwgigeqjggosgljuhliybkjpibyatofcjbfxwtalc" should return "d"', () => {
    expect(firstNonRepeatingCharacter('xdnxxlvupzuwgigeqjggosgljuhliybkjpibyatofcjbfxwtalc')).toEqual('d');
  });

  it('s: "ngrhhqbhnsipkcoqjyviikvxbxyphsnjpdxkhtadltsuxbfbrkof" should return "g"', () => {
    expect(firstNonRepeatingCharacter('ngrhhqbhnsipkcoqjyviikvxbxyphsnjpdxkhtadltsuxbfbrkof')).toEqual('g');
  });
});
