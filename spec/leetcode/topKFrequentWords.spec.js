import {topKFrequentWords} from '../../src/leetcode/topKFrequentWords';

describe('topKFrequentWords()', () => {
  it('Input: ["i", "love", "leetcode", "i", "love", "coding"] , k = 2 should Output: ["i", "love"]', () => {
    const words = ['i', 'love', 'leetcode', 'i', 'love', 'coding'];
    const k = 2;

    expect(topKFrequentWords(words, k)).toEqual(['i', 'love']);
  });

  it('Input: ["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"] , k = 4 should Output: ["the", "is", "sunny", "day"]', () => {
    const words = ["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"];
    const k = 4;

    expect(topKFrequentWords(words, k)).toEqual(["the", "is", "sunny", "day"]);
  });

  it('Input: ["i", "love", "leetcode", "a", "i", "a", "love", "coding"] , k = 2 should Output: ["a", "i"]', () => {
    const words = ["i", "love", "leetcode", "a", "i", "a", "love", "coding"];
    const k = 2;

    expect(topKFrequentWords(words, k)).toEqual(["a", "i"]);
  });
});
