import {findLongestSubarrayBySum} from '../../src/leetcode/findLongestSubarrayBySum';

describe('findLongestSubarrayBySum()', () => {
  it('arr = [1,2,3,4,5,0,0,0,6,7,8,9,10], s = 15 should return [1, 8]', () => {
    const s = 15
    const input = [1,2,3,4,5,0,0,0,6,7,8,9,10];

    expect(findLongestSubarrayBySum(input, s)).toEqual([1,8]);
  });
});
