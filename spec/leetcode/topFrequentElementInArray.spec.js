import {topFrequentElementInArray} from '../../src/leetcode/topFrequentElementInArray';

describe('topFrequentElementInArray()', () => {
  it('Input: nums = [1,1,1,2,2,3], should Output: 1', () => {
    const nums = [1,1,1,2,2,3];

    expect(topFrequentElementInArray(nums)).toEqual(3);
  });

  it('Input: nums = [1], should Output: 1', () => {
    const nums = [1];

    expect(topFrequentElementInArray(nums)).toEqual(1);
  });

  it('Input: nums = [3,0,0,1], should Output: 0', () => {
    const nums = [3,0,1,0,1];

    expect(topFrequentElementInArray(nums)).toEqual(2);
  });
});
