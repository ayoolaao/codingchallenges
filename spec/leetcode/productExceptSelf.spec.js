import {productExceptSelf, productExceptSelfAdv} from '../../src/leetcode/productExceptSelf';

describe('productExceptSelf()', () => {
  it('[1,2,3,4] should return [24,12,8,6]', () => {
    const input = [1,2,3,4];

    expect(productExceptSelf(input)).toEqual([24,12,8,6]);
  });

  it('[0, 0] should return [0, 0]', () => {
    const input = [0, 0];

    expect(productExceptSelf(input)).toEqual([0,0]);
  });
});

describe('productExceptSelfAdv()', () => {
  it('[1,2,3,4] should return [24,12,8,6]', () => {
    const input = [1,2,3,4];

    expect(productExceptSelfAdv(input)).toEqual([24,12,8,6]);
  });

  it('[0, 0] should return [0, 0]', () => {
    const input = [0, 0];

    expect(productExceptSelfAdv(input)).toEqual([0,0]);
  });
});
