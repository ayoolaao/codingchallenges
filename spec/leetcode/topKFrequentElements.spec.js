import {topKFrequent} from '../../src/leetcode/topKFrequentElements';

describe('topKFrequent()', () => {
  it('Input: nums = [1,1,1,2,2,3], k = 2 should Output: [1,2]', () => {
    const nums = [1,1,1,2,2,3];
    const k = 2;

    expect(topKFrequent(nums, k)).toEqual([1,2]);
  });

  it('Input: nums = [1], k = 1 should Output: [1]', () => {
    const nums = [1];
    const k = 1;

    expect(topKFrequent(nums, k)).toEqual([1]);
  });

  it('Input: nums = [3,0,1,0,1], k = 1 should Output: [0]', () => {
    const nums = [3,0,1,0,1];
    const k = 1;

    expect(topKFrequent(nums, k)).toEqual([0]);
  });
});
