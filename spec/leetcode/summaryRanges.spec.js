import {summaryRanges, summaryRangesAdv} from '../../src/leetcode/summaryRanges';

describe('Summary Ranges Test', () => {
  it('Input:  [0,1,2,4,5,7] should Output: ["0->2","4->5","7"]', () => {
    const inputData = [0,1,2,4,5,7];

    expect(summaryRanges(inputData)).toEqual(["0->2","4->5","7"]);
  });

  it('Input:  [0,2,3,4,6,8,9] should Output: ["0","2->4","6","8->9"', () => {
    const inputData = [0,2,3,4,6,8,9];

    expect(summaryRanges(inputData)).toEqual(["0","2->4","6","8->9"]);
  });
});


describe('Summary Ranges Test (Advanced Solution)', () => {
  it('Input:  [0,1,2,4,5,7] should Output: ["0->2","4->5","7"]', () => {
    const inputData = [0,1,2,4,5,7];

    expect(summaryRangesAdv(inputData)).toEqual(["0->2","4->5","7"]);
  });

  it('Input:  [0,2,3,4,6,8,9] should Output: ["0","2->4","6","8->9"', () => {
    const inputData = [0,2,3,4,6,8,9];

    expect(summaryRangesAdv(inputData)).toEqual(["0","2->4","6","8->9"]);
  });
});
