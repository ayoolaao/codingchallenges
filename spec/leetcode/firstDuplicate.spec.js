import {firstDuplicate, firstDuplicateAdv} from '../../src/leetcode/firstDuplicate';

describe('Test firstDuplicate()', () => {
  it('Case1: [1,2,1,2,3,3] should return 1', () => {
    const array = [1,2,1,2,3,3];

    expect(firstDuplicate(array)).toBe(1)
  });

  it('Case1: [2,1,3,5,3,2] should return 3', () => {
    const array = [2,1,3,5,3,2];

    expect(firstDuplicate(array)).toBe(3)
  });

  it('Case1: [1,2,3,4,5,6] should return -1', () => {
    const array = [1,2,3,4,5,6];

    expect(firstDuplicate(array)).toBe(-1)
  });
});

describe('Test firstDuplicateAdv()', () => {
  it('Case1: [1,2,1,2,3,3] should return 1', () => {
    const array = [1,2,1,2,3,3];

    expect(firstDuplicateAdv(array)).toBe(1)
  });

  it('Case1: [2,1,3,5,3,2] should return 3', () => {
    const array = [2,1,3,5,3,2];

    expect(firstDuplicateAdv(array)).toBe(3)
  });

  it('Case1: [1,2,3,4,5,6] should return -1', () => {
    const array = [1,2,3,4,5,6];

    expect(firstDuplicateAdv(array)).toBe(-1)
  });
});
