import { twoNumberSumBruteForce, twoNumberLeftRightPointer, twoNumberSumMap } from '../../src/algoExpert/twoNumberSum';

describe('twoNumberSumBruteForce()', () => {
  it('[3, 5, -4, 8, 11, 1, -1, 6], 10 should return [11, -1] ', () => {
    const inputArray = [3, 5, -4, 8, 11, 1, -1, 6];
    const inputTarget = 10;
    const output = [11, -1]; //? If returning the element values themselves
    // const output = [4, 6]; //? If returning the element indexes

    expect(twoNumberSumBruteForce(inputArray, inputTarget)).toEqual(output);
  });

  it('[2,7,11,15], 9 should return [2, 7] ', () => {
    const inputArray = [2, 7, 11, 15];
    const inputTarget = 9;
    const output = [2, 7]; //? If returning the element values themselves
    // const output = [0, 1]; //? If returning the element indexes

    expect(twoNumberSumBruteForce(inputArray, inputTarget)).toEqual(output);
  });
});

describe('twoNumberLeftRightPointer()', () => {
  it('[3, 5, -4, 8, 11, 1, -1, 6], 10 should return [11, -1] ', () => {
    const inputArray = [3, 5, -4, 8, 11, 1, -1, 6];
    const inputTarget = 10;
    const output = [-1, 11]; //? If returning the element values themselves
    // const output = [1, 7]; //? If returning the element indexes

    expect(twoNumberLeftRightPointer(inputArray, inputTarget)).toEqual(output);
  });

  it('[2,7,11,15], 9 should return [2, 7] ', () => {
    const inputArray = [2, 7, 11, 15];
    const inputTarget = 9;
    const output = [2, 7]; //? If returning the element values themselves
    // const output = [0, 1]; //? If returning the element indexes

    expect(twoNumberLeftRightPointer(inputArray, inputTarget)).toEqual(output);
  });
});

describe('twoNumberSumMap()', () => {
  it('[3, 5, -4, 8, 11, 1, -1, 6], 10 should return [11, -1] ', () => {
    const inputArray = [3, 5, -4, 8, 11, 1, -1, 6];
    const inputTarget = 10;
    const output = [11, -1]; //? If returning the element values themselves
    // const output = [4, 6]; //? If returning the element indexes

    expect(twoNumberSumMap(inputArray, inputTarget)).toEqual(output);
  });

  it('[2,7,11,15], 9 should return [2, 7] ', () => {
    const inputArray = [2, 7, 11, 15];
    const inputTarget = 9;
    const output = [2, 7]; //? If returning the element values themselves
    // const output = [0, 1]; //? If returning the element indexes

    expect(twoNumberSumMap(inputArray, inputTarget)).toEqual(output);
  });
});
