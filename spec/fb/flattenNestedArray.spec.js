import { flattenConcatApply, flattenReduce, flattenGenerator, flattenFlat, flatten } from '../../src/fb/flattenNestedArray';

describe('flattenConcatApply', () => {
  it('[[1, 2],[3, 4, 5], [6, 7, 8, 9]] should return [1, 2, 3, 4, 5, 6, 7, 8, 9]', () => {
    const input = [[1, 2],[3, 4, 5], [6, 7, 8, 9]];
    const output = [1, 2, 3, 4, 5, 6, 7, 8, 9];

    expect(flattenConcatApply(input)).toEqual(output)
  });
});

describe('flattenReduce', () => {
  it('[[1, 2],[3, 4, 5], [6, 7, 8, 9]] should return [1, 2, 3, 4, 5, 6, 7, 8, 9]', () => {
    const input = [[1, 2],[3, 4, 5], [6, 7, 8, 9]];
    const output = [1, 2, 3, 4, 5, 6, 7, 8, 9];

    expect(flattenReduce(input)).toEqual(output)
  });
});

xdescribe('flattenGenerator', () => {
  it('[[1, 2],[3, 4, 5], [6, 7, 8, 9]] should return [1, 2, 3, 4, 5, 6, 7, 8, 9]', () => {
    const input = [[1, 2],[3, 4, 5], [6, 7, 8, 9]];
    const output = [1, 2, 3, 4, 5, 6, 7, 8, 9];

    expect(flattenGenerator(input)).toEqual(output)
  });
});

describe('flattenFlat', () => {
  it('[[1, 2],[3, 4, 5], [6, 7, 8, 9]] should return [1, 2, 3, 4, 5, 6, 7, 8, 9]', () => {
    const input = [[1, 2],[3, 4, 5], [6, 7, 8, 9]];
    const output = [1, 2, 3, 4, 5, 6, 7, 8, 9];

    expect(flattenFlat(input)).toEqual(output)
  });
});

describe('flatten', () => {
  it('[[1, 2],[3, 4, 5], [6, 7, 8, 9]] should return [1, 2, 3, 4, 5, 6, 7, 8, 9]', () => {
    const input = [[1, 2],[3, 4, 5], [6, 7, 8, 9]];
    const output = [1, 2, 3, 4, 5, 6, 7, 8, 9];

    expect(flatten(input)).toEqual(output)
  });
});
