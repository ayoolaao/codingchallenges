import { isAPalindrome } from '../../src/misc/isAPalindrome';

describe('isAPalindrome()', () => {
  it('Case 1', () => {
    const input = '';

    expect(isAPalindrome(input)).toBeTrue();
  });

  it('Case 2', () => {
    const input = 'aoa';

    expect(isAPalindrome(input)).toBeTrue();
  });

  it('Case 3', () => {
    const input = 'aoaa';

    expect(isAPalindrome(input)).toBeFalse();
  });
});
