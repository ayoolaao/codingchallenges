import { reverseArrayBasic, } from '../../src/misc/reverseArray';

describe('reverseArrayBasic()', () => {
  it('Case 1', () => {
    expect(reverseArrayBasic([1, 2, 3])).toEqual([3, 2, 1]);
  });
});
