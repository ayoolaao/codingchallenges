import { abSolution } from '../../src/misc/abSolution';

describe('abSolution()', () => {
  it('Case 1', () => {
    const input = 'a';

    expect(abSolution(input)).toBeTrue();
  });

  it('Case 2', () => {
    const input = 'ba';

    expect(abSolution(input)).toBeFalse();
  });

  it('Case 3', () => {
    const input = 'ab';

    expect(abSolution(input)).toBeTrue();
  });

  it('Case 4', () => {
    const input = 'aabbb';

    expect(abSolution(input)).toBeTrue();
  });

  it('Case 5', () => {
    const input = 'abba';

    expect(abSolution(input)).toBeFalse();
  });

  it('Case 6', () => {
    const input = 'aaa';

    expect(abSolution(input)).toBeTrue();
  });
});
